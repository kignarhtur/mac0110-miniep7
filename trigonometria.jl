# MAC0110 - MiniEP7
# Arthur Teixeira Magalhaes - 4876102

function sin(x)

    term::BigFloat = x
    sum::BigFloat = x
    n = 2

    for i in 1 : 20

        term *= - ( x^2 ) / ( n * ( n + 1 ) )
        sum +=  term
        n += 2

    end

    return sum

end

function cos(x)

    term::BigFloat = 1
    sum::BigFloat = 1
    n = 1

    for i in 1 : 20

        term *=  - ( x^2 ) / ( n * ( n + 1 ) )
        sum += term
        n += 2

    end

    return sum

end

function bernoulli(n)

    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)

    for m = 0 : n

        A[m + 1] = 1 // (m + 1)

        for j = m : -1 : 1

            A[j] = j * (A[j] - A[j + 1])

        end

    end

    return abs(A[1])

end

function tan(x)

    denom::BigInt = 2
    mult::BigInt = 4

    term::BigFloat = x
    sum::BigFloat = x
    exp = 1
    n = 1

    for i in 2 : 20

        n += 2
        exp += 2
        mult *= 4
        denom *= n * (n + 1)

        term = mult*(mult - 1) * x^(exp) * bernoulli(i)/denom
        sum += term

    end

    return sum

end

function check_sin(value,x)

    accuracy = 1.0e-3
    difference = abs(value - sin(x))

    if difference <= accuracy
        return true

    else
        return false

    end

end

function check_cos(value,x)

    accuracy = 1.0e-3
    difference = abs(value - cos(x))

    if difference <= accuracy
        return true

    else
        return false

    end

end

function check_tan(value,x)

    accuracy = 1.0e-3
    difference = abs(value - tan(x))

    if difference <= accuracy
        return true

    else
        return false

    end

end

function taylor_sin(x)

    term::BigFloat = x
    sum::BigFloat = x
    n = 2

    for i in 1 : 20

        term *= - ( x^2 ) / ( n * ( n + 1 ) )
        sum +=  term
        n += 2

    end

    return sum

end

function taylor_cos(x)

    term::BigFloat = 1
    sum::BigFloat = 1
    n = 1

    for i in 1 : 20

        term *=  - ( x^2 ) / ( n * ( n + 1 ) )
        sum += term
        n += 2

    end

    return sum

end

function taylor_tan(x)

    denom::BigInt = 2
    mult::BigInt = 4

    term::BigFloat = x
    sum::BigFloat = x
    exp = 1
    n = 1

    for i in 2 : 20

        n += 2
        exp += 2
        mult *= 4
        denom *= n * (n + 1)

        term = mult*(mult - 1) * x^(exp) * bernoulli(i)/denom
        sum += term

    end

    return sum

end

function test()

    @test sin(π/2) == 1
    @test sin(-5π/7) == −0,03915

    @test cos(π/2) == 0
    @test cos(-5π/7) == 0,99923

    @test tan(π/4) == 0,01370
    @test tan(-5π/7) == −0,03918

    @test check_sin(0.2588,π/12) == true
    @test check_sin(0.3907,23π/180) == true
    @test check_sin(0.9205,67π/180) == true

    @test check_cos(0.9659,π/12) == true
    @test check_cos(0.9205,23π/180) == true
    @test check_cos(0.3907,67π/180) == true

    @test check_tan(0.2679,π/12) == true
    @test check_tan(0.4244,23π/180) == true
    @test check_tan(2.3558,67π/180) == true

    println("Fim dos testes.")

end
